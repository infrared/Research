# Research

Work group that is conducting research within the network.

## Goals 
To better understand:

– Infrared members’ individual and collective needs and wishes

– where Infrared can meet external needs elsewhere by developing a map of the ecosystem it works within, constituted by several like-minded and often intersecting communities.

This will allow the Infrared network to most strategically develop and implement its initiatives as a key actor and partner in the independent Internet infrastructure provider sector.

!! However, participation by Infrared members in this research project is entirely voluntary!

!! And, Infrared members comprise the research team, too so we invite you to join the research team.

So to those ends, we have to goals for the research:
 1. Scope the current composition of Infrared, highlighting values alignment, needs and requirements that can enable action within the network for future shared projects.
 2. Map the landscape of digital human rights, internet freedom, and internet governance fora and coalitions where the Infrared network has external potential for collective action.

# Admin

This research is funded by a grant so we keep track of official administrative documents in a folder named "admin".

# Work group

This research is governed by a working subgroup of Infrared. Information about the goverannce of the reserach including data stewardship, working modes, etc is saved in a folder named "wg".

# Meetings

We aim to meet every two weeks. Notes from meeting are in the meetings folder [here.](https://0xacab.org/infrared/members/Research/-/tree/no-masters/meetings?ref_type=heads)
