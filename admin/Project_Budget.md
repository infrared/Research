Total: $50,000
-------------
Admin (10% fee, project accounting, grants management, reporting, contracting): $5,000

Travel (APC member meeting, Global Gathering, NPDev Summit): $15,000

Research Methods (200 hours): $20,000

Research Review (100 hours): $10,000
