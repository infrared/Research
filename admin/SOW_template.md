_Instructions: Download the markdown file, remove these instructions, modify [Name], print to PDF, digitally sign, send to Mallory for her counter-signature along with a W9 (if US) and wire transfer details._

# Statement of Work (SOW)

This Statement of Work is an Agreement by and between Exchange Point Institute (“Organization”) and [Name] (“Researcher”).

 * Start Date: January 2024
 * End Date: December 2024

Services to Be Performed by the Researcher: Methods (Developing quantitative and qualitative research questions, conducting and analysing interviews. Desk research and landscape analysis. Report writing); and/or Review (Integrating feedback, presenting findings, producing and promoting the final report). 

Payment Terms: 

1. Fixed Price Amount: $100/hour

2. Payment within 30 days of receipt of a pre-approved timesheet.

The parties involved have signed this Statement of Work to show their agreement:

Researcher:

[Signature, date]

[Name]

Organization:

[Counter-signature, date]

Mallory Knodel, Director
Exchange Point Institute
