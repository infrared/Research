Infrared research group (IRG)

April 9, 2024

Attending: Beatrice, Brenna, Georgia, Gunner, Mallory

# Last meeting notes:
    https://0xacab.org/infrared/Research/-/blob/no-masters/meetings/2024-02-29_minutes.md?ref_type=heads

Next Meeting: April 23
    
# Agenda:
    Reconnect
    Where is stuff right now
    Roles & Responsibilities
    

# Notes:    
AG: For next general meeting (infrared) we should bring a list of needs/how we want to use it for the research group as there will be a discussion about goverance of oxacab

These meetings are set for every 2 weeks for 30min
Always on BBB: https://aspirationtech.cloud68.co/user/asp-jon-wwt-vof

0xacab.org Overview
    Project: https://0xacab.org/infrared/Research
    Research Plan: https://0xacab.org/infrared/Research/-/blob/no-masters/ResearchPlan.md?ref_type=heads

        This might be more up to date and needs to be merged into the 0xacab.org: https://docs.google.com/document/d/1ALOMQ85JUzibVtzgg3lpVhZVilryTN5hPdsmmSRytM0/edit


    Meetings Folder > For minutes
    Admin > https://0xacab.org/infrared/Research/-/tree/no-masters/admin?ref_type=heads

    Ford is pretty lightweight from a reporting perspective

    There's a budget, concept (what was given to Ford)

    TODO [UPDATED]: Clarify the Admin in the Budget: 10% fee for the 501c3 to manage the funds of the grant

# Questions for discussion:
How are the hours booked?
How many are available? If someone wants to get involved, how do they?
Hours: what is the way that hours are brokend down; who are they allocated to and how can people participate
Interested people who want to do research
SOW Template to be used as a contract for any researcher and the project
Update repo for research gropu documents

# Actions / Next Steps
    [ ] Align the Google Doc & the Research Plan in Oxacab
    [X] Update the Admin description for clarity
    [ ] Agenda Items on the 23rd: 
    Work on communications for the network
    Draft before the next call to review
    Work on how to use the budget for the 
    List of open questions
        What are we 
        [ ] Roles & Responsibilities
        [X] Fix 0xacab access
        Admin: spell out what this 


Budget





https://0xacab.org/infrared/Research/-/blob/no-masters/admin/SOW_template.md?ref_type=heads


